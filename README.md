# SpareSti - frontend
## 1. Introduction

Welcome to [SpareSti](https://sparesti.com/)! The leading tool to reach your saving goals! Big or small. It doesnt matter. [SpareSti](https://sparesti.com/) has you covered!

This application was developed as a school project for subject IDATT2106 at NTNU in the spring semester of 2024.

## 2. Technologies used

### 2.1 Vue 3 + Vite

This template should help get you started developing with Vue 3 in Vite. The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

### 2.X Other technologies
- Boilerplate components: Shadcn vue (https://www.shadcn-vue.com/docs/components/accordion.html)
- CSS Framework: TailwindCSS (https://tailwindcss.com/)
- Icons: Lucide icons (https://lucide.dev/icons/)
- Testing: Vitest (https://vitest.dev/guide/)
- State management: Pinia (https://pinia.vuejs.org/core-concepts/)
- Git Commit convention (https://ec.europa.eu/component-library/v1.15.0/eu/docs/conventions/git/)


## Getting started
### 3.1 Installation
Clone project from Gitlap repo.

Clone with SSH:
git@gitlab.stud.idi.ntnu.no:idatt2106_2024_01/frontend.git

Clone with HTTPS:
https://gitlab.stud.idi.ntnu.no/idatt2106_2024_01/frontend.git

Go to you directory folder and run:
```bash
yarn install
```

### 3.2 Run


#### Terminal
After installation run:
```bash
yarn dev
```
in terminal.

#### IDE 
Open the project in a suitable IDE. (e.g. Webstorm or VS code) and press the "run" button.
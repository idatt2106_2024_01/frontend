describe('Create saving goal test', () => {
    beforeEach(() => {
        window.localStorage.setItem('access_token',
            'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIzIiwiaXNzIjoiZnN0X3Rva2VuX2lzc3Vlcl9hc' +
            'HAiLCJleHAiOjE3MjIyMzk3MTQsImlhdCI6MTcxNDQ2MzcxNH0.AjYqAXgFKnhzeOBOTICVciiKpvOGDGyfQq0--' +
            'YuA0GlLSsplrETK1AbaQTYWnMPBJPF2MGWsbRESN1g-DTEeKg');
        cy.visit('http://localhost:5173/create-saving-goal');
    });

    it('displays the form elements correctly', () => {
        cy.get('form').within(() => {
            cy.contains('Hva vil du spare til').should('be.visible')
            cy.contains('Hvor mye vil du spare (NOK)').should('be.visible')
            cy.contains('Når vil du nå målet?').should('be.visible')
            cy.contains('Avbryt').should('be.visible')
            cy.contains('Lagre').should('be.visible')
        })
    })

    it('displays error messages for invalid form submission', () => {
        cy.get('form').within(() => {
            cy.get('button').contains('Lagre').click()
            cy.contains('Tittel er påkrevd').should('be.visible')
            cy.contains('Beløp må være større enn 0').should('be.visible')
            cy.contains('Dato må være valgt').should('be.visible')

            cy.get('input[name="title"]').click()
            cy.get('input[name="amount"]').type('0')
            cy.get('button').contains('Lagre').click()

            cy.contains('Tittel er påkrevd').should('be.visible')
            cy.contains('Beløp må være større enn 0').should('be.visible')
            cy.contains('Dato må være valgt').should('be.visible')
        })
    })

    it('allows user to cancel the form', () => {
        cy.get('form').within(() => {
            cy.get('button').contains('Avbryt').click()
        })

        cy.url().should('include', '/dashboard')
    })

    it('allows user to submit the form', () => {
        it('allows user to submit the form', () => {
            cy.get('form').within(() => {
                cy.get('input[name="title"]').type('Ferie')
                cy.get('input[name="amount"]').type('10000')

                cy.get('button').contains('Velg dato').click()

                cy.get('.react-datepicker__month').find('.react-datepicker__day')
                    .then($days => {
                        const todayIndex = $days.toArray().findIndex(day => day.className
                            .includes('react-datepicker__day--today'));
                        const tomorrowIndex = todayIndex + 1;
                        cy.wrap($days[tomorrowIndex]).click();
                    });
                cy.get('button').contains('Lagre').click()
            })
        })
    })

});

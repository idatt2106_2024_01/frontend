describe('Create saving goal test', () => {
    beforeEach(() => {
        window.localStorage.setItem('access_token',
            'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIzIiwiaXNzIjoiZnN0X3Rva2VuX2lzc3Vlcl9hc' +
            'HAiLCJleHAiOjE3MjIyMzk3MTQsImlhdCI6MTcxNDQ2MzcxNH0.AjYqAXgFKnhzeOBOTICVciiKpvOGDGyfQq0--' +
            'YuA0GlLSsplrETK1AbaQTYWnMPBJPF2MGWsbRESN1g-DTEeKg');
        cy.visit('http://localhost:5173/dashboard');
    });

    it('renders without crashing', () => {
        cy.get('body').should('be.visible')
    })

    it('renders the status medaljer button', () => {
        cy.get('[type="button"]').should('be.visible')
    })

    it('displayes the status for medaljer', () => {
        cy.get('[type="button"]').click()
        cy.get('#radix-vue-popover-content-1').should('be.visible')
    })

    it('renders link to medaljer page', () => {
        cy.get('[href="/badges"]')
    })

    it('renders link to spareoversikt page', () => {
        cy.get('[href="/saving-overview"]')
    })
})
describe ('OnboardingView', () => {
    beforeEach(() => {
        window.localStorage.setItem('access_token',
            'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIzIiwiaXNzIjoiZnN0X3Rva2VuX2lzc3Vlcl9hc' +
            'HAiLCJleHAiOjE3MjIyMzk3MTQsImlhdCI6MTcxNDQ2MzcxNH0.AjYqAXgFKnhzeOBOTICVciiKpvOGDGyfQq0--' +
            'YuA0GlLSsplrETK1AbaQTYWnMPBJPF2MGWsbRESN1g-DTEeKg');
        cy.visit('http://localhost:5173/generate-challenges');
    });

    it('renders without crashing', () => {
        cy.get('body').should('be.visible');
    });

    it('renders title', () => {
        cy.get('.pt-7').should('be.visible');
    })

    it('displays loading indicator when loading', () => {
        cy.get('img[alt="Loading"]').should('be.visible')
    })

});
describe ('OnboardingView', () => {
    beforeEach(() => {
        window.localStorage.setItem('access_token',
            'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIzIiwiaXNzIjoiZnN0X3Rva2VuX2lzc3Vlcl9hc' +
            'HAiLCJleHAiOjE3MjIyMzk3MTQsImlhdCI6MTcxNDQ2MzcxNH0.AjYqAXgFKnhzeOBOTICVciiKpvOGDGyfQq0--' +
            'YuA0GlLSsplrETK1AbaQTYWnMPBJPF2MGWsbRESN1g-DTEeKg');
        cy.visit('http://localhost:5173/onboarding');
    });

    it('renders without crashing', () => {
        cy.get('body').should('be.visible');
    });

    it('renders the onboarding form', () => {
        cy.get('form').should('be.visible');
    });

    it('checkbox is disabled when email is not filled in', () => {
        cy.get('#radix-3-form-item').click();
        cy.get('input[type="checkbox"]').should('be.disabled');
    })

    it('checkbox is disabled when invalid email is filled in', () => {
        cy.get('#radix-3-form-item').click().type('TEst');
        cy.get('input[type="checkbox"]').should('be.disabled');
    })

    it('checkbox is enabled when valid email is filled in', () => {
        cy.get('#radix-3-form-item').click().type('maria@banan.no');
        cy.get('input[type="checkbox"]').should('not.be.disabled');
    })

    it('submits the form when clicking the submit button', () => {
        cy.get('button').contains('Neste').click();
        cy.get('button').contains('Neste').click();
        cy.get('button').contains('Neste').click();
        cy.get('.justify-between.pt-4 > .bg-primary-dark').click();
        cy.url().should('include', '/dashboard');
    })

    it('goes back to the previous question when clicking the previous button', () => {
        cy.get('button').contains('Neste').click();
        cy.get('button').contains('Tilbake').click();
        cy.get('.p-4').should('be.visible');
    })
});
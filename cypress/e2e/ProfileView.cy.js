describe('Innloggingsprosess', () => {
    beforeEach(() => {
        window.localStorage.setItem('access_token',
            'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIzIiwiaXNzIjoiZnN0X3Rva2VuX2lzc3Vlcl9hc' +
            'HAiLCJleHAiOjE3MjIyMzk3MTQsImlhdCI6MTcxNDQ2MzcxNH0.AjYqAXgFKnhzeOBOTICVciiKpvOGDGyfQq0--' +
            'YuA0GlLSsplrETK1AbaQTYWnMPBJPF2MGWsbRESN1g-DTEeKg');
        cy.visit('http://localhost:5173/profile');
    });

    it('renders without crashing', () => {
        cy.get('h1').contains('MIN PROFIL').should('be.visible')
    })

    it('renders the profile form', () => {
        cy.get('form').should('be.visible');
    });

    it('renders the lagre button', () => {
        cy.get('button').contains('Lagre').should('be.visible');
    })

    it('renders the logg ut button', () => {
        cy.get('button').contains('Logg ut').should('be.visible');
    })

    it('can edit the name', () => {
        cy.get('#radix-2-form-item').type('Test');
    })

    it('can edit the email', () => {
        cy.get('#radix-3-form-item').type('Test');
    })

    it('can toggle the toggle item',() =>{
        cy.get('#radix-5-form-item > [value="A_LOT"]').click();
    })

})
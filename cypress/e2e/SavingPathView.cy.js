describe('SavingPathView tests', () => {
    beforeEach(() => {
        window.localStorage.setItem('access_token',
            'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIzIiwiaXNzIjoiZnN0X3Rva2VuX2lzc3Vlcl9hc' +
            'HAiLCJleHAiOjE3MjIyMzk3MTQsImlhdCI6MTcxNDQ2MzcxNH0.AjYqAXgFKnhzeOBOTICVciiKpvOGDGyfQq0--' +
            'YuA0GlLSsplrETK1AbaQTYWnMPBJPF2MGWsbRESN1g-DTEeKg');
        cy.visit('http://localhost:5173/saving-path');
    });

    it('renders without crashing', () => {
        cy.get('body').should('be.visible')
    })

    it('renders title', () => {
        cy.get('h1').should('be.visible')
    })

    it('renders dropdown for time sort', () => {
        cy.get('[aria-controls="radix-vue-select-content-1"]').should('be.visible')
    })

    it('renders dropdown for difficulty', () => {
        cy.get('[aria-controls="radix-vue-select-content-2"]').should('be.visible')
    })

})
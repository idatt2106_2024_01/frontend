describe ('TransactionsView', () => {
    beforeEach(() => {
        window.localStorage.setItem('access_token',
            'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIzIiwiaXNzIjoiZnN0X3Rva2VuX2lzc3Vlcl9hc' +
            'HAiLCJleHAiOjE3MjIyMzk3MTQsImlhdCI6MTcxNDQ2MzcxNH0.AjYqAXgFKnhzeOBOTICVciiKpvOGDGyfQq0--' +
            'YuA0GlLSsplrETK1AbaQTYWnMPBJPF2MGWsbRESN1g-DTEeKg');
        cy.visit('http://localhost:5173/transactions');
    });

    it('renders without crashing', () => {
        cy.get('body').should('be.visible');
    });

    it('renders the transactions table', () => {
        cy.get('table').should('be.visible');
    });

    it('opens the add transaction dialog when clicking the plus sign button', () => {
        cy.get('.dialog-content').should('not.exist');

        cy.get('.add-transaction-button').click();
    });

    it('name of transaction is clickable', () => {
        cy.get(':nth-child(4) > :nth-child(2)').click();
    });

});
import { shallowMount } from '@vue/test-utils';
import { describe, it, expect, beforeEach } from 'vitest';
import AcceptPreviewChallenge from '@/components/AcceptPreviewChallenge.vue';
import { createPinia, setActivePinia } from 'pinia';

/**
 * Testing the AcceptPreviewChallenge component
 */
describe('Choose Saving Challenge: Component testing', () => {
  let wrapper;
  const savingChallengeMock = {
    savingChallengeId: 1,
    description: 'Test challenge',
    category: {
      categoryId: 1,
      name: 'Test Category'
    },
    currentSpending: 100,
    targetSpending: 50,
    startDate: '2024-04-22T00:00:00.000+00:00',
    endDate: '2024-04-25T00:00:00.000+00:00',
    status: 'ACTIVE',
    difficulty: 'MEDIUM'
  };

  beforeEach(() => {
    setActivePinia(createPinia());
    wrapper = shallowMount(AcceptPreviewChallenge, {
      global: {
        stubs: ['RouterLink']
      },
      props: {
        savingChallenge: savingChallengeMock
      }
    });
  });

  it('renders the component', () => {
    expect(wrapper.exists()).toBe(true);
  });

  it('updates time and target spending when updateTime is called', async () => {
    await wrapper.vm.updateTime(7);
    expect(wrapper.vm.time).toBe(7);
    expect(wrapper.vm.targetSpending).toBe(savingChallengeMock.currentSpending);

    await wrapper.vm.updateTime(14);
    expect(wrapper.vm.time).toBe(14);
    expect(wrapper.vm.targetSpending).toBe(
      savingChallengeMock.currentSpending * 2
    );

    await wrapper.vm.updateTime(30);
    expect(wrapper.vm.time).toBe(30);
    expect(wrapper.vm.targetSpending).toBe(
      savingChallengeMock.currentSpending * 4
    );
  });
});

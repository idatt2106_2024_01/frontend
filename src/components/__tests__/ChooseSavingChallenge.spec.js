import { mount } from '@vue/test-utils';
import { describe, it, expect } from 'vitest';
import ChooseSavingChallenge from '@/components/ChooseSavingChallenge.vue';

/**
 * Testing the ChooseSavingChallenge component
 */
describe('Choose Saving Challenge: Component testing', () => {
  it('renders the component', () => {
    const wrapper = mount(ChooseSavingChallenge);
    expect(wrapper.exists()).toBe(true);
  });
});

import { mount } from '@vue/test-utils';
import { describe, beforeEach, it, expect } from 'vitest';
import ConfigCategories from '@/components/ConfigCategories.vue';
import { createPinia, setActivePinia } from 'pinia';

/**
 * Testing the ConfigCategories component
 */
describe('ConfigCategories.vue', () => {
  let wrapper;

  beforeEach(() => {
    setActivePinia(createPinia());
    wrapper = mount(ConfigCategories, {
      global: {
        stubs: [
          'Card',
          'Button',
          'FormField',
          'FormItem',
          'FormControl',
          'Dialog'
        ]
      },
      propsData: {
        values: {
          categories: []
        },
        setFieldValue: () => {}
      }
    });
  });

  it('renders the component', () => {
    expect(wrapper.exists()).toBe(true);
  });

  it('renders the categories', () => {
    expect(wrapper.vm.categories).toEqual([]);
  });
});

import { shallowMount } from '@vue/test-utils';
import { describe, it, expect, beforeEach } from 'vitest';
import CurrentSavingChallenge from '@/components/CurrentSavingChallenge.vue';
import { createPinia, setActivePinia } from 'pinia';

/**
 * Testing the CurrentSavingChallenge component
 */
describe('Current Saving Challenge: Component testing', () => {
  beforeEach(() => {
    setActivePinia(createPinia());
  });
  it('renders the component with required props', () => {
    const savingChallengeMock = {
      description: 'Test challenge',
      category: {
        categoryId: 1,
        name: 'Test Category'
      },
      currentSpending: 100,
      targetSpending: 50,
      startDate: '2024-04-22T00:00:00.000+00:00',
      endDate: '2024-04-25T00:00:00.000+00:00',
      status: 'ACTIVE',
      difficulty: 'MEDIUM'
    };

    const wrapper = shallowMount(CurrentSavingChallenge, {
      props: {
        savingChallenge: savingChallengeMock
      }
    });
    expect(wrapper.exists()).toBe(true);
  });
});

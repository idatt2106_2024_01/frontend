import { mount } from '@vue/test-utils';
import { describe, it, expect } from 'vitest';
import FooterComponent from '../FooterComponent.vue';
import { createPinia, setActivePinia } from 'pinia';

/**
 * Testing the FooterComponent component
 */
describe('FooterComponent', () => {
  it('rendedrs without crashing', () => {
    setActivePinia(createPinia());
    const wrapper = mount(FooterComponent, {
      global: {
        stubs: ['RouterLink']
      }
    });
    expect(wrapper.exists()).toBe(true);
  });
});

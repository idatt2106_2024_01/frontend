import { mount } from '@vue/test-utils';
import { describe, it, expect, beforeEach } from 'vitest';
import NavBar from '@/components/NavBar.vue';
import { createPinia, setActivePinia } from 'pinia';
import { createRouter, createMemoryHistory } from 'vue-router';

/**
 * Testing the NavBar component
 */

/**
 * Mocking the router
 */
const routes = [{ path: '/', component: NavBar }];

/**
 * Create a mock router
 */
const createMockRouter = () => {
  return createRouter({
    history: createMemoryHistory(),
    routes
  });
};

describe('NavBar', () => {
  let router;

  beforeEach(async () => {
    setActivePinia(createPinia());
    router = createMockRouter();
    router.push('/');
    await router.isReady();
  });
  it('rendedrs without crashing', () => {
    setActivePinia(createPinia());
    const wrapper = mount(NavBar, {
      global: {
        plugins: [router],
        stubs: ['RouterLink']
      }
    });
    expect(wrapper.exists()).toBe(true);
  });

  it('toggles navOpen on click', async () => {
    setActivePinia(createPinia());
    const wrapper = mount(NavBar, {
      global: {
        plugins: [router],
        stubs: ['RouterLink']
      }
    });
    const button = wrapper.find('button');
    await button.trigger('click');
    expect(wrapper.vm.navOpen).toBe(true);
    await button.trigger('click');
    expect(wrapper.vm.navOpen).toBe(false);
  });

  it('updates isMobileMenu on window resize', async () => {
    setActivePinia(createPinia());
    const wrapper = mount(NavBar, {
      global: {
        plugins: [router],
        stubs: ['RouterLink']
      }
    });
    global.innerWidth = 500;
    global.dispatchEvent(new Event('resize'));
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.isMobileMenu).toBe(true);
    global.innerWidth = 800;
    global.dispatchEvent(new Event('resize'));
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.isMobileMenu).toBe(false);
  });
});

import { mount } from '@vue/test-utils';
import { describe, beforeEach, it, expect } from 'vitest';
import { createPinia, setActivePinia } from 'pinia';
import OnboardingProfile from '@/components/OnboardingProfile.vue';

/**
 * Testing the OnboardingProfile component
 */

describe('Testing of OnboardingProfile component', () => {
  let wrapper;

  beforeEach(() => {
    setActivePinia(createPinia());
    wrapper = mount(OnboardingProfile, {
      global: {
        stubs: [
          'Card',
          'Button',
          'FormField',
          'FormItem',
          'FormControl',
          'Dialog'
        ]
      },
      propsData: {
        setFieldValue: () => {},
        errors: [],
        values: {}
      }
    });
  });

  it('renders the component', () => {
    expect(wrapper.exists()).toBe(true);
  });

  it('returns true when isEmailValidForConsent is called with a valid email', () => {
    wrapper.vm.values.email = 'test@example.com';
    expect(wrapper.vm.isEmailValidForConsent()).toBe(true);
  });

  it('returns false when isEmailValidForConsent is called with an invalid email', () => {
    wrapper.vm.values.email = 'invalid-email';
    expect(wrapper.vm.isEmailValidForConsent()).toBe(false);
  });

  it('returns false when isEmailValidForConsent is called with an empty email', () => {
    wrapper.vm.values.email = '';
    expect(wrapper.vm.isEmailValidForConsent()).toBe(false);
  });

  it('returns false when isEmailValidForConsent is called with an email missing the domain', () => {
    wrapper.vm.values.email = 'missingdomain@';
    expect(wrapper.vm.isEmailValidForConsent()).toBe(false);
  });

  it('returns false when isEmailValidForConsent is called with an email missing the username', () => {
    wrapper.vm.values.email = '@missingusername.com';
    expect(wrapper.vm.isEmailValidForConsent()).toBe(false);
  });
});

import { mount } from '@vue/test-utils';
import { describe, beforeEach, it, expect } from 'vitest';
import { createPinia, setActivePinia } from 'pinia';
import OnboardingQuestions from '@/components/OnboardingQuestions.vue';

/**
 * Testing the OnboardingQuestions component
 */
describe('Testing of OnboardingProfile component', () => {
  let wrapper;

  beforeEach(() => {
    setActivePinia(createPinia());
    wrapper = mount(OnboardingQuestions, {
      global: {
        stubs: [
          'Card',
          'Button',
          'FormField',
          'FormItem',
          'FormControl',
          'Dialog',
          'ToggleGroup'
        ]
      },
      propsData: {
        setFieldValue: () => {},
        errors: [],
        values: {}
      }
    });
  });

  it('renders the component', () => {
    expect(wrapper.exists()).toBe(true);
  });
});

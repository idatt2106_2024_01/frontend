import NewSavingPath from '@/components/SavingPath.vue';
import { createPinia, setActivePinia } from 'pinia';
import { describe, beforeEach, it, expect } from 'vitest';
import { mount } from '@vue/test-utils';

/**
 * Testing the SavingPath component
 */
describe('Saving Path: Component testing', () => {
  let wrapper;

  const savingGoalMock = {
    savingGoalId: 1,
    title: 'Test goal',
    goal: 1000,
    deadline: '2024-04-22T00:00:00.000+00:00',
    status: 'ACTIVE'
  };

  beforeEach(() => {
    setActivePinia(createPinia());
    wrapper = mount(NewSavingPath, {
      global: {
        stubs: ['Popover', 'PopoverTrigger', 'PopoverContent']
      },
      props: {
        savingGoal: savingGoalMock
      }
    });
    wrapper.vm.coinThresholds = {
      14: 1000
    };
  });

  it('renders the component', () => {
    expect(wrapper.exists()).toBe(true);
  });

  it('should correctly identify diagonal indices', () => {
    expect(wrapper.vm.isDiagonal(8)).toBe(true);
    expect(wrapper.vm.isDiagonal(16)).toBe(true);
    expect(wrapper.vm.isDiagonal(7)).toBe(false);
    expect(wrapper.vm.isDiagonal(21)).toBe(false);
  });

  it('should update clickedButtons and openedPopover when a button is clicked', async () => {
    wrapper.vm.clickButton(5);
    await wrapper.vm.$nextTick();

    expect(wrapper.vm.clickedButtons[4]).toBe(true);
    expect(wrapper.vm.openedPopover).toBe(5);
  });
});

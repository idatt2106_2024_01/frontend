import { mount } from '@vue/test-utils';
import { describe, it, expect, beforeEach } from 'vitest';
import Sparti from '@/components/Sparti.vue';
import { createPinia, setActivePinia } from 'pinia';
import { useThemeStore } from '@/stores/theme.js';

/**
 * Testing the Sparti component
 */
describe('Sparti component testing', () => {
  let wrapper;
  beforeEach(() => {
    setActivePinia(createPinia());
    wrapper = mount(Sparti, {
      propsData: {
        theme: 'theme1',
        text: 'Test text'
      }
    });
  });

  it('renders the component', () => {
    expect(wrapper.exists()).toBe(true);
  });

  it('renders PinkMascot image when theme is theme1', () => {
    useThemeStore().theme = 'theme1';
    expect(wrapper.find('img').attributes('src')).toContain('PinkMascot.png');
  });
});

import { mount } from '@vue/test-utils';
import { describe, it, expect, beforeEach } from 'vitest';
import SuggestedSavingChallenge from '@/components/SuggestedSavingChallenge.vue';
import { createPinia, setActivePinia } from 'pinia';

/**
 * Testing the SuggestedSavingChallenge component
 */
describe('Suggested Saving Challenge: Component testing', () => {
  beforeEach(() => {
    setActivePinia(createPinia());
  });
  it('renders the component without crashing', () => {
    const savingChallengeMock = {
      description: 'Test challenge',
      category: {
        categoryId: 1,
        name: 'Test Category'
      },
      currentSpending: 100,
      targetSpending: 50,
      startDate: '2024-04-22T00:00:00.000+00:00',
      endDate: '2024-04-25T00:00:00.000+00:00',
      status: 'ACTIVE',
      difficulty: 'MEDIUM'
    };

    const wrapper = mount(SuggestedSavingChallenge, {
      props: {
        savingChallenge: savingChallengeMock,
        targetSpending: savingChallengeMock.targetSpending
      }
    });

    expect(wrapper.exists()).toBe(true);
  });
});

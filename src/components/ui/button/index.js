import { cva } from 'class-variance-authority';

export { default as Button } from './Button.vue';

export const buttonVariants = cva(
  'inline-flex items-center justify-center whitespace-nowrap rounded-md text-sm font-medium ' +
    'ring-offset-white transition-colors focus-visible:outline-none focus-visible:ring-2 ' +
    'focus-visible:ring-slate-950 focus-visible:ring-offset-2 disabled:pointer-events-none disabled:opacity-50 ' +
    'dark:ring-offset-slate-950 dark:focus-visible:ring-slate-300',
  {
    variants: {
      variant: {
        default:
          'bg-secondary-dark text-slate-50 hover:bg-primary-dark text-white',
        destructive: 'bg-red-500 text-slate-50 hover:bg-red-500/80',
        outline: 'border-2 bg-white hover:border-darkest hover:text-black',
        icons:
          'bg-primary-dark text-white hover:bg-secondary-dark hover:shadow-lg',
        primary:
          'bg-primary-dark text-white hover:bg-secondary-dark hover:text-darkest hover:shadow-lg',
        secondary:
          'bg-primary text-darkest hover:bg-secondary-light hover:shadow-lg',
        tertiary:
          'bg-secondary text-white hover:bg-secondary-light hover:text-darkest hover:shadow-lg',
        quaternary:
          'border-2 border-primary-dark bg-standard text-primary-dark hover:bg-primary-dark hover:text-standard',
        ghost: '',
        link: 'text-slate-900 underline-offset-4 hover:underline',
        transferSavings:
          'h-60 text-lg bg-primary text-slate-50 hover:bg-primary-dark',
        none: ''
      },
      size: {
        default: 'h-10 px-4 py-2',
        sm: 'h-10 rounded-md px-3',
        lg: 'h-12 rounded-md px-8',
        icon: 'h-10 w-10',
        none: ''
      }
    },
    defaultVariants: {
      variant: 'default',
      size: 'default'
    }
  }
);

import { describe, it, expect, vi } from 'vitest';
import { mount } from '@vue/test-utils';
import { createApp, ref } from 'vue';
import {
  useProvideCarousel,
  useCarousel
} from '/src/components/ui/carousel/useCarousel.js'; // Adjust the path as necessary

// Mock the emblaCarouselVue import
vi.mock('embla-carousel-vue', () => {
  const on = vi.fn();
  const scrollPrev = vi.fn();
  const scrollNext = vi.fn();
  const canScrollNext = () => true;
  const canScrollPrev = () => true;
  const api = {
    on,
    scrollPrev,
    scrollNext,
    canScrollNext,
    canScrollPrev
  };

  return {
    default: () => [
      ref(), // emblaNode
      ref(api) // emblaApi
    ]
  };
});

describe('Carousel Logic', () => {
  it('throws error when useCarousel is used outside of Carousel provider', () => {
    expect(() => {
      mount({
        template: '<div></div>',
        setup() {
          return useCarousel();
        }
      });
    }).toThrow('useCarousel must be used within a <Carousel />');
  });

  it('scrollPrev and scrollNext methods are callable', async () => {
    const wrapper = mount({
      template: '<div><div ref="carouselRef"></div></div>',
      setup() {
        const { scrollPrev, scrollNext } = useProvideCarousel(
          {
            opts: {},
            orientation: 'horizontal',
            plugins: []
          },
          vi.fn()
        );

        return { scrollPrev, scrollNext };
      }
    });

    await wrapper.vm.scrollPrev();
    await wrapper.vm.scrollNext();
    // Assertions to check if the methods are callable and working as expected
  });
});

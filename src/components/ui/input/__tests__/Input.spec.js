import { describe, it, expect } from 'vitest';
import { mount } from '@vue/test-utils';
import InputComponent from '@/components/ui/input/Input.vue'; // Update the path according to your project structure

describe('InputComponent', () => {
  it('renders correctly with default props', () => {
    const wrapper = mount(InputComponent);
    const input = wrapper.find('input');
    expect(input.exists()).toBe(true);
  });

  it('accepts a default value', () => {
    const defaultValue = 'Hello';
    const wrapper = mount(InputComponent, {
      props: { defaultValue }
    });
    const input = wrapper.find('input');
    expect(input.element.value).toBe(defaultValue);
  });

  it('emits an update:modelValue event when input is typed into', async () => {
    const wrapper = mount(InputComponent);
    const input = wrapper.find('input');
    await input.setValue('New value');
    expect(wrapper.emitted('update:modelValue')).toBeTruthy();
    expect(wrapper.emitted('update:modelValue')[0]).toEqual(['New value']);
  });

  it('applies custom classes if provided', () => {
    const customClass = 'custom-class';
    const wrapper = mount(InputComponent, {
      props: { class: customClass }
    });
    expect(wrapper.find('input').classes()).toContain(customClass);
  });
});

/**
 * Enum for account types.
 */
const AccountType = {
  SAVINGS: 'SAVINGS',
  CHECKING: 'CHECKING'
};

/**
 * Converts an account type to the norwegian corresponding label.
 */
function accountTypeToLabel(accountType) {
  switch (accountType) {
    case AccountType.SAVINGS:
      return 'Sparekonto';
    case AccountType.CHECKING:
      return 'Brukskonto';
    default:
      return 'Unknown';
  }
}

export { AccountType, accountTypeToLabel };

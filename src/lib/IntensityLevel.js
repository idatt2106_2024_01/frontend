/**
 * Enum for IntensityLevel
 */
const IntensityLevel = {
  A_LITTLE: 'A_LITTLE',
  SOME: 'SOME',
  A_LOT: 'A_LOT'
};

/**
 * Converts an IntensityLevel to the corresponding label.
 */
function IntensityLevelToEnum() {
  return Object.values(IntensityLevel);
}

export { IntensityLevel, IntensityLevelToEnum };

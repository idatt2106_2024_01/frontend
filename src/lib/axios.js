import axios from 'axios';
const baseURL = import.meta.env.VITE_REST_API_URL;

const axiosInstance = axios.create({
  baseURL: baseURL
});

/**
 * Configures the axios instance to include the access token in the Authorization header
 */
axiosInstance.interceptors.request.use(
  (config) => {
    config.headers.Authorization = `Bearer ${localStorage.getItem('access_token')}`;
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default axiosInstance;

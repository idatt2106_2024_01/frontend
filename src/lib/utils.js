import { clsx } from 'clsx';
import { twMerge } from 'tailwind-merge';

/**
 * Function to merge tailwindcss classes with clsx classes
 *
 * @param  {...any} inputs
 * @returns {string} merged tailwind classes
 */
export function cn(...inputs) {
  return twMerge(clsx(inputs));
}

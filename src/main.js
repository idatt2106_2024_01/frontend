import { createApp } from 'vue';
import './style.css';
import App from './App.vue';
import router from './router';
import { createPinia } from 'pinia';
import { useUserStore } from './stores/user';
import { useThemeStore } from './stores/theme';
import Particles from '@tsparticles/vue3';
import { loadFull } from 'tsparticles';

/**
 * Main file for the Vue app
 * This file creates the app, sets up the router, and mounts the app to the DOM
 */

/**
 * Creates a Pinia store
 */
const pinia = createPinia();

/**
 * Creates a new Vue app
 */
const app = createApp(App);

/**
 * Passes the router to the app
 */
app.use(router);

/**
 * Passes the Pinia store to the app
 */
app.use(pinia);

/**
 * Passes the Particles plugin to the app
 */
app.use(Particles, {
  init: async (engine) => {
    await loadFull(engine);
  }
});

/**
 * The store for the user
 */
const userStore = useUserStore();

/**
 * The store for the theme
 */
const themeStore = useThemeStore();

/**
 * If the user has an access token, get the user info
 */
if (localStorage.getItem('access_token')) {
  userStore.getUserInfo();
}

/**
 * If the theme is saved in local storage, set the theme
 */
if (localStorage.getItem('theme')) {
  themeStore.setTheme(localStorage.getItem('theme'));
}

/**
 * Mounts the app to the DOM
 */
app.mount('#app');

import { createWebHistory, createRouter } from 'vue-router';

import TransactionView from '@/views/TransactionView.vue';
import DashboardView from '@/views/DashboardView.vue';
import ProfileView from '@/views/ProfileView.vue';
import SavingPathView from '@/views/SavingPathView.vue';
import GenerateChallengesView from '@/views/GenerateChallengesView.vue';
import PreviewChallengeView from '@/views/PreviewChallengeView.vue';
import BadgesView from '@/views/BadgesView.vue';
import SavingOverviewView from '@/views/SavingOverviewView.vue';
import CreateSavingGoalView from '@/views/CreateSavingGoalView.vue';
import AuthView from '@/views/AuthView.vue';
import OnboardingView from '@/views/OnboardingView.vue';

/**
 * Define the routes of the application
 */
const routes = [
  { path: '/', component: DashboardView },
  {
    path: '/auth',
    component: AuthView,
    meta: {
      isPublic: true
    }
  },
  { path: '/transactions', component: TransactionView },
  { path: '/dashboard', component: DashboardView },
  { path: '/profile', component: ProfileView },
  { path: '/saving-path', component: SavingPathView },
  { path: '/generate-challenges', component: GenerateChallengesView },
  { path: '/badges', component: BadgesView },
  { path: '/saving-overview', component: SavingOverviewView },
  { path: '/create-saving-goal', component: CreateSavingGoalView },
  { path: '/onboarding', component: OnboardingView },
  {
    path: '/preview-challenge/:id',
    name: 'PreviewChallenge',
    component: PreviewChallengeView,
    props: true
  }
];

/**
 * Create the router instance
 */
const router = createRouter({
  history: createWebHistory(),
  routes
});

/**
 * Add a navigation guard to check if the user is authenticated
 */
router.beforeEach((to, from, next) => {
  console.log('to', to);
  if (!to.meta.isPublic && !localStorage.getItem('access_token')) {
    console.log('redirecting to /auth');
    return next('/auth');
  }
  next();
});

export default router;

import { describe, beforeEach, it, expect, vi } from 'vitest';
import { createPinia, setActivePinia } from 'pinia';
import { useBadgeStore } from '@/stores/badge';
import axios from '@/lib/axios';

/**
 * Tests for the Badge Store
 */

/**
 * Mocking the axios module
 */
vi.mock('@/lib/axios', () => ({
  default: {
    get: vi.fn((url) => {
      switch (url) {
        case '/badges':
          return Promise.resolve({
            data: [
              {
                badgeId: 1,
                name: 'Badge 1',
                description: 'Description 1',
                url: 'https://example.com/badge1.png'
              },
              {
                badgeId: 2,
                name: 'Badge 2',
                description: 'Description 2',
                url: 'https://example.com/badge2.png'
              }
            ]
          });
        case '/users/badges/achieved':
          return Promise.resolve({
            data: [{ badgeId: 2 }]
          });
        default:
          return Promise.reject(new Error('Not Found'));
      }
    })
  }
}));

describe('Badge Store', () => {
  let badgeStore;

  beforeEach(() => {
    setActivePinia(createPinia());
    badgeStore = useBadgeStore();
  });

  it('should start with empty arrays for badges and achieved badges', () => {
    expect(badgeStore.badges).toEqual([]);
    expect(badgeStore.achievedBadges).toEqual([]);
  });

  it('should fetch badges and update store state', async () => {
    await badgeStore.fetchBadges();
    expect(badgeStore.badges).toEqual([
      {
        badgeId: 1,
        name: 'Badge 1',
        description: 'Description 1',
        url: 'https://example.com/badge1.png'
      },
      {
        badgeId: 2,
        name: 'Badge 2',
        description: 'Description 2',
        url: 'https://example.com/badge2.png'
      }
    ]);
    expect(axios.get).toHaveBeenCalledWith('/badges');
  });

  it('should fetch achieved badges and update store state', async () => {
    await badgeStore.fetchAchievedBadges();
    expect(badgeStore.achievedBadges).toEqual([2]);
    expect(axios.get).toHaveBeenCalledWith('/users/badges/achieved');
  });
});

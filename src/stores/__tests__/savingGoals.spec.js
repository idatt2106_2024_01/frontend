import { describe, beforeEach, it, expect, vi } from 'vitest';
import { createPinia, setActivePinia } from 'pinia';
import { useSavingGoalsStore } from '@/stores/savingGoals';
import axios from '@/lib/axios';

/**
 * Tests for the Saving Goals Store
 */

/**
 * Mocking the axios module
 */
vi.mock('@/lib/axios', () => ({
  default: {
    get: vi.fn(() =>
      Promise.resolve({
        data: [{ title: 'Goal title', goal: 2000, deadline: '2026-11-30' }]
      })
    )
  }
}));

describe('Saving Goals Store', () => {
  let savingGoalsStore;

  beforeEach(() => {
    setActivePinia(createPinia());
    savingGoalsStore = useSavingGoalsStore();
  });
  it('should have an empty list of saving goals', () => {
    expect(savingGoalsStore.savingGoals).toEqual([]);
  });

  it('should fetch saving goals and update store state', async () => {
    await savingGoalsStore.fetchSavingGoals();
    expect(savingGoalsStore.savingGoals).toEqual([
      {
        title: 'Goal title',
        goal: 2000,
        deadline: '2026-11-30'
      }
    ]);
    expect(axios.get).toHaveBeenCalled();
    expect(axios.get).toHaveBeenCalledWith('/saving-goals');
  });
});

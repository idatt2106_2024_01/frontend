import { setActivePinia, createPinia } from 'pinia';
import { useUserStore } from '../user.js';
import { describe, test, expect, beforeEach } from 'vitest';

/**
 * Tests for the User Store
 */
describe('User Store', () => {
  let userStore;

  beforeEach(() => {
    setActivePinia(createPinia());
    userStore = useUserStore();
  });

  test('it should have a default user', () => {
    expect(userStore.user).toEqual({
      userId: 1,
      firstName: 'John',
      lastName: 'Doe',
      email: '',
      newsletterConsent: false,
      nickname: 'T',
      isOnboarded: true,
      profileAvatar: 'https://api.sparesti.no/images/avatar/StdAvatar.png'
    });
  });
});

import { defineStore } from 'pinia';
import axios from '@/lib/axios.js';

/**
 * Badge Store
 * Manages and handles operations related to badges
 * The store contains state for badges, achieved badges, and user stats
 */

/**
 * Defines the badge store
 * */
const useBadgeStore = defineStore('badge', {
  state: () => ({
    badges: [],
    achievedBadges: [],
    userStats: {
      totalSavings: 0,
      hardChallengesCompleted: 0,
      successfulStreak: 0
    }
  }),
  actions: {
    /**
     * Fetches the badges from the API
     */
    async fetchBadges() {
      try {
        const response = await axios.get('/badges');
        if (Array.isArray(response.data)) {
          this.badges = response.data;
        } else {
          console.error(
            'Expected an array of badges, but received:',
            response.data
          );
          this.badges = [];
        }
      } catch (error) {
        console.error('Failed to fetch badges', error);
      }
    },

    /**
     * Fetches the badges that the user has achieved
     */
    async fetchAchievedBadges() {
      try {
        const response = await axios.get('/users/badges/achieved');
        if (Array.isArray(response.data)) {
          this.achievedBadges = response.data.map((badge) => badge.badgeId);
        } else {
          console.error(
            'Expected an array of achieved badges, but received:',
            response.data
          );
          this.achievedBadges = [];
        }
      } catch (error) {
        console.error('Failed to fetch achieved badges', error);
        this.achievedBadges = [];
      }
    },

    /**
     * Fetches the user stats from the API
     */
    async fetchUserStats() {
      try {
        const response = await axios.get('/users/stats');
        this.userStats = response.data;
        console.log('User stats fetched successfully:', this.userStats);
      } catch (error) {
        console.error('Error fetching user stats:', error);
        throw new Error('Failed to fetch user stats');
      }
    }
  }
});

export { useBadgeStore };

import { defineStore } from 'pinia';
import axios from '@/lib/axios';

/**
 * Category Store
 * Manages and handles operations related to categories
 * The store contains state for categories
 */

/**
 * Defines the category store
 * */
const useCategoryStore = defineStore('category', {
  state: () => ({
    categories: []
  }),
  getters: {
    currentCategory: (state) => state.categories[0] || null
  },

  actions: {
    /**
     * Fetch categories
     */
    async fetchCategories() {
      try {
        const response = await axios.get('/categories');
        this.categories = response.data;
        return response.data;
      } catch (error) {
        console.error('Failed to fetch categories:', error);
      }
    },
    /**
     * Create category
     */
    async createCategory(categoryDTO) {
      try {
        const response = await axios.post('/categories', categoryDTO);
        this.categories.push(response.data);
        return response.data.categoryId;
      } catch (error) {
        console.error('Failed to create category:', error);
      }
    },
    /**
     * Delete category
     */
    async deleteCategory(categoryId) {
      try {
        await axios.delete(`/categories/${categoryId}`);
        this.categories = this.categories.filter(
          (category) => category.id !== categoryId
        );
      } catch (error) {
        console.error('Failed to delete category:', error);
      }
    }
  }
});

export { useCategoryStore };

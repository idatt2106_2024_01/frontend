import { defineStore } from 'pinia';
import axios from '@/lib/axios.js';

/**
 * Saving Challenge Store
 * Manages and handles operations related to saving challenges
 * The store contains state for saving challenges
 */

/**
 * Defines the saving challenge store
 */
const useSavingChallengeStore = defineStore('savingChallenge', {
  state: () => ({
    loading: true,
    savingChallenges: [],
    activeSavingChallenges: [],
    completedSavingChallenges: [],
    failedSavingChallenges: [],
    suggestedSavingChallenges: []
  }),

  getters: {
    /**
     * Returns the number of active saving challenges
     */
    activeChallengesCount() {
      return this.activeSavingChallenges.length;
    },

    /**
     * Returns the number of completed saving challenges
     */
    completedChallengesCount() {
      return this.completedSavingChallenges.length;
    },

    /**
     * Returns the number of failed saving challenges
     */
    failedChallengesCount() {
      return this.failedSavingChallenges.length;
    }
  },

  actions: {
    /**
     * Fetch saving challenges
     */
    async fetchSavingChallenges() {
      try {
        const response = await axios.get('/saving-challenges');
        this.savingChallenges = response.data;
        this.activeSavingChallenges = response.data.filter(
          (sc) => sc.status === 'ACTIVE'
        );
        this.completedSavingChallenges = response.data.filter(
          (sc) => sc.status === 'COMPLETED'
        );
        this.failedSavingChallenges = response.data.filter(
          (sc) => sc.status === 'FAILED'
        );
        this.loading = false;
      } catch (error) {
        console.error('Failed to fetch saving challenges:', error);
      }
    },

    /**
     * Creates a new saving challenge
     */
    async createActiveSavingChallenge(savingChallengeDTO) {
      try {
        const response = await axios.post(
          '/saving-challenges',
          savingChallengeDTO
        );
        this.activeSavingChallenges.push(response.data);
      } catch (error) {
        console.error('Failed to create saving challenge:', error);
      }
    },

    /**
     * Fetch suggested saving challenges
     */
    async fetchSuggestedSavingChallenges() {
      try {
        const response = await axios.get('/saving-challenges/generate');
        this.suggestedSavingChallenges = response.data;
      } catch (error) {
        console.error('Failed to fetch saving challenges:', error);
      }
    },

    /**
     * Fetch saving challenge by id
     */
    async fetchActiveSavingChallengeById(id) {
      try {
        const response = await axios.get(`/saving-challenges/${id}`);
        console.log(response.data);
        return response.data;
      } catch (error) {
        console.error('Failed to fetch saving challenge:', error);
      }
    },

    /**
     * Removes a saving challenge by id
     */
    async removeSavingChallenge(id) {
      try {
        await axios.delete(`/saving-challenges/${id}`);
        this.savingChallenges = this.savingChallenges.filter(
          (sc) => sc.id !== id
        );
        this.activeSavingChallenges = this.activeSavingChallenges.filter(
          (sc) => sc.id !== id
        );
        this.completedSavingChallenges = this.completedSavingChallenges.filter(
          (sc) => sc.id !== id
        );
        this.failedSavingChallenges = this.failedSavingChallenges.filter(
          (sc) => sc.id !== id
        );
      } catch (error) {
        console.error('Failed to remove saving challenge:', error);
      }
    },

    /**
     * Updates a saving challenge by id
     */
    async updateSavingChallenge(savingChallengeId, data) {
      try {
        const response = await axios.put(
          `/saving-challenges/${savingChallengeId}`,
          data
        );
        const index = this.activeSavingChallenges.findIndex(
          (sc) => sc.savingChallengeId === savingChallengeId
        );
        if (index !== -1) {
          this.activeSavingChallenges[index] = response.data;
        }
        console.log('Saving challenge updated successfully:', response.data);
      } catch (error) {
        console.error('Failed to update saving challenge:', error);
      }
    },

    /**
     * Fetches a saving tip
     */
    async fetchSavingTip(category) {
      try {
        const response = await axios.get(
          `/saving-tip/generate?category=${category}`
        );
        return response.data;
      } catch (error) {
        console.error('Failed to fetch saving tip:', error);
        return null;
      }
    }
  }
});

export { useSavingChallengeStore };

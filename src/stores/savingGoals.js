import { defineStore } from 'pinia';
import axios from '@/lib/axios';

/**
 * Saving Goals Store
 * Manages and handles operations related to saving goals
 * The store contains state for loading, saving goals, active saving goals, completed saving goals, failed saving goals, and total saved by goal
 */

/**
 * Defines the saving goals store
 */
const useSavingGoalsStore = defineStore('savingGoals', {
  state: () => ({
    loading: true,
    savingGoals: [],
    activeSavingGoals: [],
    completedSavingGoals: [],
    failedSavingGoals: [],
    totalSavedByGoal: {}
  }),
  getters: {
    /**
     * Gets the current saving goal
     */
    currentSavingGoal: (state) => state.savingGoals[0] || null,

    /**
     * Gets the number of active saving goals
     */
    activeSavingGoalsCount() {
      return this.activeSavingGoals.length;
    },

    /**
     * Gets the number of completed saving goals
     */
    completedSavingGoalsCount() {
      return this.completedSavingGoals.length;
    },

    /**
     * Gets the number of failed saving goals
     */
    failedSavingGoalsCount() {
      return this.failedSavingGoals.length;
    },

    /**
     * Gets the total saved for a saving goal
     */
    getTotalSavedByGoal: (state) => (savingGoalId) => {
      return state.totalSavedByGoal[savingGoalId];
    }
  },

  actions: {
    /**
     * Fetch saving goals
     */
    async fetchSavingGoals() {
      try {
        const response = await axios.get('/saving-goals');
        this.savingGoals = response.data;
        this.loading = false;
        this.activeSavingGoals = response.data.filter(
          (sc) => sc.status === 'ACTIVE'
        );
        this.completedSavingGoals = response.data.filter(
          (sc) => sc.status === 'COMPLETED'
        );
        this.failedSavingGoals = response.data.filter(
          (sc) => sc.status === 'FAILED'
        );
      } catch (error) {
        console.error('Failed to fetch saving goals:', error);
      }
    },
    /**
     * Create saving goal
     */
    async createSavingGoal(savingGoalDTO) {
      try {
        const response = await axios.post('/saving-goals', savingGoalDTO);
        this.savingGoals.push(response.data);
      } catch (error) {
        console.error('Failed to create saving goal:', error);
      }
    },

    /**
     * Fetch total saved for a saving goal
     */
    async fetchTotalSavedBySavingGoalId(savingGoalId) {
      try {
        const response = await axios.get(
          `/saving-goals/${savingGoalId}/total-saved`
        );
        this.totalSavedByGoal[savingGoalId] = response.data;
      } catch (error) {
        console.error('Failed to fetch total saved for saving goal:', error);
      }
    },

    /**
     * Check if total saved for a saving goal is already fetched
     */
    async checkAndFetchTotalSaved(savingGoalId) {
      if (!this.totalSavedByGoal.hasOwnProperty(savingGoalId)) {
        await this.fetchTotalSavedBySavingGoalId(savingGoalId);
      }
    }
  }
});

export { useSavingGoalsStore };

import { defineStore } from 'pinia';

/**
 * Theme store
 * Manages and handles operations related to the theme
 */

/**
 * Defines the theme store
 */
const useThemeStore = defineStore('theme', {
  state: () => ({
    theme: 'theme1'
  }),
  actions: {
    /**
     * Sets the theme
     */
    setTheme(newTheme) {
      this.theme = newTheme;
      localStorage.setItem('theme', newTheme);
      document.documentElement.className = newTheme;
    }
  }
});

export { useThemeStore };

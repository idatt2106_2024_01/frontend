import { defineStore } from 'pinia';
import axios from '@/lib/axios';

/**
 * Transactions store
 * Manages and handles operations related to transactions
 * The store contains state for transactions, savings transactions, and savings transactions with saving goal
 */

/**
 * Defines the transactions store
 */
const useTransactionsStore = defineStore('transactions', {
  state: () => ({
    transactions: [],
    savingsTransactions: [],
    savingsTransactionsWithSavingGoal: []
  }),
  getters: {
    /**
     * Gets the total saved from all savings transactions
     */
    getTotalSaved: (state) =>
      state.savingsTransactions.reduce((acc, t) => acc + t.totalSaved, 0),

    /**
     * Gets the total saved from all savings transactions with a saving goal
     */
    getTotalSavedWithSavingGoal: (state) =>
      state.savingsTransactionsWithSavingGoal.reduce(
        (acc, t) => acc + t.totalSaved,
        0
      )
  },
  actions: {
    /**
     * Fetches transactions for a given bank account
     */
    async fetchTransactions(accountId, page = 1) {
      try {
        const response = await axios.get(
          `/bank-accounts/${accountId}/transactions`,
          {
            params: { page: page - 1 }
          }
        );
        if (response.data && Array.isArray(response.data.content)) {
          this.transactions = response.data.content;
          return {
            totalPages: response.data.totalPages || 0,
            currentPage: response.data.number + 1 // Correct for zero indexing
          };
        } else {
          throw new Error('Invalid response structure from API');
        }
      } catch (error) {
        console.error('Error fetching transactions:', error);
        this.transactions = [];
        return { totalPages: 0, currentPage: 1 };
      }
    },

    /**
     * Adds a transaction to a bank account
     */
    async addTransaction(transactionData, accountId) {
      try {
        const response = await axios.post(
          `/bank-accounts/${accountId}/transactions`,
          transactionData
        );
        this.transactions.push(response.data);
      } catch (error) {
        console.error('Failed to add transaction:', error);
        throw error;
      }
    },

    /**
     * Updates a transaction for a bank account
     */
    async updateTransaction(accountId, transactionId, updateData) {
      console.log(accountId, transactionId, updateData);
      try {
        const response = await axios.put(
          `/bank-accounts/${accountId}/transactions/${transactionId}`,
          updateData
        );
        const updatedTransaction = response.data;
        const index = this.transactions.findIndex(
          (t) => t.transactionId === transactionId
        );
        if (index !== -1) {
          this.transactions[index] = {
            ...this.transactions[index],
            ...updatedTransaction
          };
        }
      } catch (error) {
        console.error('Failed to update transaction:', error);
        throw error;
      }
    },

    /**
     * Fetches saving transactions for the last year by each month
     */
    async fetchSavingTransactionsForLastYearByEachMonth() {
      try {
        const response = await axios.get(
          '/bank-accounts/1/transactions/last-year'
        );
        this.savingsTransactions = response.data.all;
        this.savingsTransactionsWithSavingGoal =
          response.data.onlyFromSavingGoal;
      } catch (error) {
        console.error(
          'Failed to fetch saving transactions for last year by each month:',
          error
        );
        return { error: error };
      }
    }
  }
});

export { useTransactionsStore };

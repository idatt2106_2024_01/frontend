import { defineStore } from 'pinia';
import axios from '@/lib/axios';
import { accountTypeToLabel } from '@/lib/AccountType';

/**
 * User store
 * Manages and handles operations related to the user
 * The store contains state for the user, checking account, and savings account
 */

/**
 * Defines the user store
 */
const useUserStore = defineStore('user', {
  state: () => ({
    user: {
      userId: 1,
      firstName: 'John',
      lastName: 'Doe',
      email: '',
      nickname: 'T',
      isOnboarded: true,
      profileAvatar: 'https://api.sparesti.no/images/avatar/StdAvatar.png',
      newsletterConsent: false
    },
    checkingAccount: null,
    savingsAccount: null
  }),

  getters: {
    /**
     * Checks if the user is logged in
     */
    loggedIn: (state) => !!state.user,

    /**
     * Gets the user's display name
     */
    displayName: (state) =>
      state.user.userPreferences?.nickname
        ? state.user.userPreferences.nickname
        : state.user.firstName,

    /**
     * Gets the user's profile avatar
     */
    displayProfileAvatar: (state) => state.user.profileAvatar
  },
  actions: {
    /**
     * Logs the user out
     */
    async logout() {
      localStorage.removeItem('access_token');
      window.location.href = '/auth';
    },

    /**
     * Gets the user's information
     */
    async getUserInfo() {
      try {
        const response = await axios.get('/users/me');
        const { bankAccounts, ...user } = response.data;
        this.user = user;
        this.checkingAccount = bankAccounts.find(
          (account) => account.accountType === 'CHECKING'
        );
        this.savingsAccount = bankAccounts.find(
          (account) => account.accountType === 'SAVINGS'
        );
      } catch (error) {
        console.error(error);
        this.logout();
      }
    },

    /**
     * Adds a bank account to the user
     */
    async integrateWithBank(accountNumber, accountType) {
      try {
        const response = await axios.post('/bank-accounts/integrate', {
          name: accountTypeToLabel(accountType),
          accountNumber,
          accountType
        });
        if (accountType === 'CHECKING') {
          this.checkingAccount = response.data;
        } else if (accountType === 'SAVINGS') {
          this.savingsAccount = response.data;
        }
        return 'Account added successfully';
      } catch (error) {
        console.error(error);
        return 'Failed to add account';
      }
    },

    /**
     * Adds a profile avatar to the user
     */
    async addProfileAvatar(profileAvatar) {
      try {
        await axios.patch('/users/me/profile-avatar', {
          profileAvatar
        });
        this.user.profileAvatar = profileAvatar;
        return 'Profile avatar added successfully';
      } catch (error) {
        console.error('Failed to add profile avatar');
      }
    },

    /**
     * Gets the user's profile avatar
     */
    async getProfileAvatar() {
      try {
        const response = await axios.get('/users/me/profile-avatar');
        return response.data;
      } catch (error) {
        console.error('Failed to get profile avatar');
      }
    },

    /**
     * Updates the user's newsletter consent
     */
    async updateNewsletterConsent(newsletterConsent) {
      try {
        await axios.patch('/users/me/newsletter-consent', {
          newsletterConsent
        });
        this.user.newsletterConsent = newsletterConsent;
        return 'Newsletter consent updated successfully';
      } catch (error) {
        console.error('Failed to update newsletter consent');
      }
    },

    /**
     * Updates the user's email
     */
    async updateEmail(email) {
      try {
        await axios.put('/users/me/email', {
          email
        });
        this.user.email = email;
        return 'Email updated successfully';
      } catch (error) {
        console.error('Failed to update email');
      }
    },

    /**
     * Updates the name of a bank account
     */
    async updateAccountName(name, accountType, accountNumber) {
      try {
        await axios.put(`/bank-accounts/${accountNumber}`, {
          name
        });
        if (accountType === 'CHECKING') {
          this.checkingAccount.name = name;
        } else if (accountType === 'SAVINGS') {
          this.savingsAccount.name = name;
        }
        return 'Account name updated successfully';
      } catch (error) {
        console.error('Failed to update account name');
      }
    },

    /**
     * Adds a new user preference
     */
    async newUserPreferences(savingLevel, willingnessToInvest) {
      try {
        await axios.put('/users/user-preferences', {
          savingLevel,
          willingnessToInvest
        });
        this.user.userPreferences.savingLevel = savingLevel;
        this.user.userPreferences.willingnessToInvest = willingnessToInvest;
        return 'User preferences updated successfully';
      } catch (error) {
        console.error('Failed to update user preferences');
      }
    },

    /**
     * Updates the user's preferred saving categories
     */
    async updateUserCategories(savingCategories) {
      try {
        await axios.put('/users/user-preferences/saving-categories', {
          savingCategories
        });
        this.user.userPreferences.savingCategories = savingCategories;
      } catch (error) {
        console.error('Failed to update user categories');
      }
    },

    /**
     * Updates the user's information
     */
    async updateUserInfo(userInfoDTO) {
      try {
        await axios.put('/users', userInfoDTO);
        this.user = { ...this.user, ...userInfoDTO };
        return 'User info updated successfully';
      } catch (error) {
        console.error('Failed to update user info');
      }
    },

    /**
     * Updates the user's preferences
     */
    async updateUserPreferences(userPreferencesDTO) {
      try {
        await axios.put('/users/user-preferences', userPreferencesDTO);
        this.user.userPreferences = {
          ...this.user.userPreferences,
          ...userPreferencesDTO
        };
        return 'User preferences updated successfully';
      } catch (error) {
        console.error('Failed to update user preferences');
      }
    }
  }
});

export { useUserStore };

/**
 * Enum for difficulty levels.
 */
const Difficulty = {
  EASY: 'EASY',
  MEDIUM: 'MEDIUM',
  HARD: 'HARD'
};

/**
 * Generates an array of difficulty levels with value and label in Norwegian.
 */
function getDifficulties() {
  return Object.keys(Difficulty).map((key) => ({
    value: Difficulty[key],
    label: getDifficultyLabel(Difficulty[key])
  }));
}

/**
 * Returns the Norwegian label of a given difficulty level.
 */
function getDifficultyLabel(level) {
  switch (level) {
    case Difficulty.EASY:
      return 'Lett';
    case Difficulty.MEDIUM:
      return 'Middels';
    case Difficulty.HARD:
      return 'Vanskelig';
    default:
      return 'Ukjent';
  }
}

/**
 * Exports the Difficulty object, getDifficulties, and getDifficultyLabel functions.
 */
export { Difficulty, getDifficulties, getDifficultyLabel };

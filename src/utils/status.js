/**
 * Enum for status of a task
 */
const Status = {
  COMPLETED: 'COMPLETED',
  ACTIVE: 'ACTIVE',
  FAILED: 'FAILED'
};

/**
 * Returns the Norwegian label of a given status
 */
function getStatus(status) {
  switch (status) {
    case Status.COMPLETED:
      return 'Fullført';
    case Status.ACTIVE:
      return 'Aktiv';
    case Status.FAILED:
      return 'Feilet';
    default:
      return 'Ukjent';
  }
}

/**
 * Exports the Status object and getStatus function.
 */
export { Status, getStatus };

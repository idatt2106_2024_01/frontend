/**
 * Utility function to calculate the time remaining for a given date
 */

/**
 * Sets the time remaining for a given end date
 */
export function setTimeRemaining(
  endDateElement,
  daysLeftElement,
  hoursLeftElement
) {
  const localEndDate = new Date(endDateElement.value);
  const offset = localEndDate.getTimezoneOffset();
  const now = new Date();
  now.setMinutes(now.getMinutes() - offset);
  const total = localEndDate.getTime() - now.getTime();

  if (total < 0) {
    daysLeftElement.value = 0;
    hoursLeftElement.value = 0;
    return;
  }

  const hours = Math.floor((total / (1000 * 60 * 60)) % 24);
  const days = Math.floor(total / (1000 * 60 * 60 * 24));
  hoursLeftElement.value = hours;
  daysLeftElement.value = days;
}

import { mount } from '@vue/test-utils';
import AuthView from '@/views/AuthView.vue';
import { describe, it, expect, beforeEach } from 'vitest';
import { createPinia, setActivePinia } from 'pinia';

/**
 * Tests for the AuthView
 */
describe('AuthView.vue', () => {
  let wrapper;

  beforeEach(() => {
    setActivePinia(createPinia());
    wrapper = mount(AuthView, {
      global: {
        mocks: {
          $route: {
            query: {}
          }
        }
      }
    });
  });

  it('renders the component', () => {
    expect(wrapper.exists()).toBe(true);
  });

  it('renders a logo image', () => {
    expect(wrapper.find('img').exists()).toBe(true);
  });
});

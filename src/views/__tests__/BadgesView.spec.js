import { describe, it, expect, beforeEach, vi } from 'vitest';
import { createTestingPinia } from '@pinia/testing';
import { mount } from '@vue/test-utils';
import BadgesView from '@/views/BadgesView.vue';
import { createRouter, createMemoryHistory } from 'vue-router';
import { useBadgeStore } from '@/stores/badge.js';
import { PopoverContent } from 'radix-vue';

/**
 * Test for the BadgesView
 */

/**
 * Define the routes for the test
 */
const routes = [{ path: '/badges', component: BadgesView }];

/**
 * Creates a mock router
 */
const createMockRouter = () => {
  return createRouter({
    history: createMemoryHistory(),
    routes
  });
};

describe('BadgesView.vue', () => {
  let router;
  let wrapper;

  beforeEach(async () => {
    const pinia = createTestingPinia({
      createSpy: vi.fn,
      stubActions: false
    });

    router = createMockRouter();
    await router.push('/badges');
    await router.isReady();
    wrapper = mount(BadgesView, {
      global: {
        plugins: [router, pinia],
        stubs: { RouterLink: true, Button: true }
      }
    });

    const badgeStore = useBadgeStore();
    badgeStore.$state = {
      badges: [
        {
          badgeId: 1,
          url: 'https://api.sparesti.no/images/Coin500.png',
          name: 'Spart 500 kr',
          description: 'Totalt spart 500 kroner i sparemål',
          achieved: true
        },
        {
          badgeId: 2,
          url: 'https://api.sparesti.no/images/Coin1000.png',
          name: 'Spart 1000 kr',
          description: 'Totalt spart 1000 kroner i sparemål',
          achieved: true
        },
        {
          badgeId: 3,
          url: 'https://api.sparesti.no/images/Coin10000.png',
          name: 'Spart 10 000 kr',
          description: 'Totalt spart 10 000 kroner i sparemål',
          achieved: false
        }
      ],
      achievedBadges: [1, 2]
    };

    badgeStore.fetchBadges = vi.fn(() => Promise.resolve());
    badgeStore.fetchAchievedBadges = vi.fn(() => Promise.resolve());
  });

  it('renders without crashing', () => {
    expect(wrapper.exists()).toBe(true);
  });

  it('renders 3 badges', () => {
    const imgElements = wrapper.findAll('img');
    expect(imgElements.length).toBe(3);
  });

  it('renders a popover content for each badge plus tooltip popover', () => {
    const popoverContents = wrapper.findAllComponents(PopoverContent);
    expect(popoverContents.length).toBe(4);
  });

  it('applies grayscale correctly based on achievement for badge 3', () => {
    const imgElements = wrapper.findAll('img');
    const imgElement = imgElements.at(2);
    expect(imgElement.classes()).toContain('grayscale');
  });
});

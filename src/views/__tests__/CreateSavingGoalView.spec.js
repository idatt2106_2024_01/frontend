import { describe, it, expect, beforeEach } from 'vitest';
import CreateSavingGoalView from '@/views/CreateSavingGoalView.vue';
import { mount } from '@vue/test-utils';
import { createPinia, setActivePinia } from 'pinia';
import { createRouter, createMemoryHistory } from 'vue-router';

/**
 * Test for the CreateSavingGoalView
 */

/**
 * Define the routes for the test
 */
const routes = [
  { path: '/create-saving-goal', component: CreateSavingGoalView }
];

/**
 * Creates a mock router
 */
const createMockRouter = () => {
  return createRouter({
    history: createMemoryHistory(),
    routes
  });
};

describe('CreateSavingGoalView.vue', () => {
  let router;
  let wrapper;

  beforeEach(async () => {
    setActivePinia(createPinia());
    router = createMockRouter();
    await router.push('/create-saving-goal');
    await router.isReady();
    wrapper = mount(CreateSavingGoalView, {
      global: {
        plugins: [router],
        stubs: ['RouterLink', 'Button']
      }
    });
  });

  it('renders without crashing', () => {
    expect(wrapper.exists()).toBe(true);
  });

  it('renders the form fields correctly', () => {
    const formFields = wrapper.findAll('input');
    expect(formFields.length).toBe(3);
  });

  it('renders the form with initial empty values', () => {
    const formFields = wrapper.findAll('input');
    formFields.forEach((field) => {
      expect(field.element.value).toBe('');
    });
  });

  it('renders the form with correct labels', () => {
    const labels = wrapper.findAll('label');
    expect(labels[0].text()).toBe('Hva vil du spare til?');
    expect(labels[1].text()).toBe('Hvor mye vil du spare (NOK)?');
    expect(labels[2].text()).toBe('Når vil du nå målet?');
  });

  it('renders the form with correct placeholders', () => {
    const formFields = wrapper.findAll('input');
    expect(formFields[0].attributes('placeholder')).toBe('Reise til Spania');
    expect(formFields[1].attributes('placeholder')).toBe('15 000');
    expect(formFields[2].attributes('placeholder')).toBeUndefined();
  });

  it('leaves the form empty when the button is clicked', async () => {
    const formFields = wrapper.findAll('input');
    const button = wrapper.find('button');
    await button.trigger('click');
    formFields.forEach((field) => {
      expect(field.element.value).toBe('');
    });
  });

  it('prevents form submission when the form is invalid', async () => {
    const form = wrapper.find('form');
    await form.trigger('submit.prevent');
    expect(wrapper.emitted('submit')).toBeFalsy();
  });
});

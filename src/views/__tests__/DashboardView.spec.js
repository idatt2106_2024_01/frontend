import { mount } from '@vue/test-utils';
import DashboardView from '@/views/DashboardView.vue';
import { describe, it, expect, beforeEach } from 'vitest';
import { createPinia, setActivePinia } from 'pinia';
import { createRouter, createMemoryHistory } from 'vue-router';

/**
 * Test for the DashboardView
 */

/**
 * Define the routes for the test
 */
const routes = [{ path: '/dashboard', component: DashboardView }];

/**
 * Creates a mock router
 */
const createMockRouter = () => {
  return createRouter({
    history: createMemoryHistory(),
    routes
  });
};

describe('Testing of DashboardView', () => {
  let router;

  beforeEach(async () => {
    setActivePinia(createPinia());
    router = createMockRouter();
    router.push('/dashboard');
    await router.isReady();
  });
  it('renders without crashing', () => {
    const wrapper = mount(DashboardView, {
      global: {
        plugins: [router],
        stubs: ['RouterLink', 'Card']
      }
    });
    expect(wrapper.exists()).toBe(true);
  });

  it('renders Card title within each Card component', () => {
    const wrapper = mount(DashboardView, {
      global: {
        plugins: [router],
        stubs: ['RouterLink']
      }
    });
    const cards = wrapper.findAllComponents({ name: 'Card' });
    cards.forEach((card) => {
      expect(card.findComponent({ name: 'CardTitle' }).exists()).toBe(true);
    });
  });

  it('renders an image within each Card component', () => {
    const wrapper = mount(DashboardView, {
      global: {
        plugins: [router],
        stubs: ['RouterLink']
      }
    });
    const cards = wrapper.findAllComponents({ name: 'Card' });
    cards.forEach((card) => {
      expect(card.find('img').exists()).toBe(true);
    });
  });
});

import { mount } from '@vue/test-utils';
import { describe, it, expect, beforeEach } from 'vitest';
import GenerateChallengesView from '@/views/GenerateChallengesView.vue';
import { Dialog } from '@/components/ui/dialog/index.js';
import { ArrowLeft } from 'lucide-vue-next';
import { createPinia, setActivePinia } from 'pinia';
import { createRouter, createMemoryHistory } from 'vue-router';
import suggestedSavingChallenge from '@/components/SuggestedSavingChallenge.vue';
import DashboardView from '@/views/DashboardView.vue';

/**
 * Test for the GenerateChallengesView
 */

/**
 * Define the routes for the test
 */
const routes = [
  { path: '/generate-challenges', component: GenerateChallengesView },
  { path: '/', component: DashboardView }
];

/**
 * Creates a mock router
 */
const createMockRouter = () => {
  return createRouter({
    history: createMemoryHistory(),
    routes
  });
};

describe('Tests for GenerateChallengesView', () => {
  let router;

  beforeEach(async () => {
    setActivePinia(createPinia());
    router = createMockRouter();
    await router.push('/generate-challenges');
    await router.isReady();
  });
  let wrapper;
  beforeEach(async () => {
    wrapper = mount(GenerateChallengesView, {
      global: {
        plugins: [router],
        stubs: ['RouterLink', ArrowLeft, Dialog, suggestedSavingChallenge]
      }
    });
  });

  it('renders without crashing', () => {
    expect(wrapper.exists()).toBe(true);
  });

  it('renders title', () => {
    const title = wrapper.find('h1');
    expect(title.text()).toBe('Laster inn...');
  });

  it('displays loading indicator when loading', async () => {
    expect(
      wrapper.find('img[alt="Mynter som stables mens siden laster"]').exists()
    ).toBe(true);
  });
});

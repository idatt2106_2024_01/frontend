import { expect, it, describe, beforeEach } from 'vitest';
import { mount } from '@vue/test-utils';
import OnboardingView from '@/views/OnboardingView.vue';
import { createPinia, setActivePinia } from 'pinia';

/**
 * Test for the OnboardingView component
 */
describe('Testing of OnboardingView', () => {
  let router;
  beforeEach(async () => {
    setActivePinia(createPinia());
  });
  it('renders without crashing', () => {
    const wrapper = mount(OnboardingView, {
      global: {
        stubs: [
          'RouterLink',
          'Input',
          'Button',
          'Card',
          'CardTitle',
          'CardText'
        ]
      }
    });
    expect(wrapper.exists()).toBe(true);
  });

  it('navigates to the previous step when clicking "Tilbake"', async () => {
    const wrapper = mount(OnboardingView, {
      global: {
        stubs: [
          'RouterLink',
          'Input',
          'Button',
          'Card',
          'CardTitle',
          'CardText'
        ]
      },
      mocks: {
        $router: router
      },
      data() {
        return {
          step: 2
        };
      }
    });
  });

  it('decrements step value when clicking "Tilbake"', async () => {
    const wrapper = mount(OnboardingView, {
      global: {
        stubs: [
          'RouterLink',
          'Input',
          'Button',
          'Card',
          'CardTitle',
          'CardText'
        ]
      },
      mocks: {
        $router: router
      },
      data() {
        return {
          step: 2
        };
      }
    });

    const backButton = wrapper.find('[type="button"]');
    await backButton.trigger('click');

    expect(wrapper.vm.step).toBe(1);
  });

  it('does not decrement step value below 1 when clicking "Tilbake"', async () => {
    const wrapper = mount(OnboardingView, {
      global: {
        stubs: [
          'RouterLink',
          'Input',
          'Button',
          'Card',
          'CardTitle',
          'CardText'
        ]
      },
      mocks: {
        $router: router
      },
      data() {
        return {
          step: 1
        };
      }
    });

    const backButton = wrapper.find('[type="button"]');
    await backButton.trigger('click');

    expect(wrapper.vm.step).toBe(1);
  });
});

import { mount } from '@vue/test-utils';
import PreviewChallengeView from '@/views/PreviewChallengeView.vue';
import { describe, it, expect, beforeEach } from 'vitest';
import { createPinia, setActivePinia } from 'pinia';
import { createRouter, createMemoryHistory } from 'vue-router';

/**
 * Test for PreviewChallengeView
 */

/**
 * Defines the routes for the test
 */
const routes = [
  { path: '/preview-challenge', component: PreviewChallengeView }
];

/**
 * Creates a mock router
 */
const createMockRouter = () => {
  return createRouter({
    history: createMemoryHistory(),
    routes
  });
};

describe('Testing of PreviewChallengeView', () => {
  let router;

  beforeEach(async () => {
    setActivePinia(createPinia());
    router = createMockRouter();
    router.push('/preview-challenge');
    await router.isReady();
  });
  it('renders without crashing', () => {
    const wrapper = mount(PreviewChallengeView, {
      global: {
        plugins: [router],
        stubs: ['RouterLink', 'SuggestedSavingChallenge']
      },
      data() {
        return {
          goal: 100,
          amount: 50
        };
      }
    });
    expect(wrapper.exists()).toBe(true);
  });

  it('renders with correct initial data', () => {
    const wrapper = mount(PreviewChallengeView, {
      global: {
        plugins: [router],
        stubs: ['RouterLink', 'SuggestedSavingChallenge']
      },
      data() {
        return {
          goal: 100,
          amount: 50
        };
      }
    });
    expect(wrapper.vm.goal).toBe(100);
    expect(wrapper.vm.amount).toBe(50);
  });

  it('Enables button when time is up and spent is less than or equal to goal', () => {
    const wrapper = mount(PreviewChallengeView, {
      global: {
        plugins: [router],
        stubs: ['RouterLink', 'SuggestedSavingChallenge']
      },
      data() {
        return {
          goal: 100,
          amount: 50,
          daysLeft: 0,
          hoursLeft: 0,
          spent: 100
        };
      }
    });
    const button = wrapper.find('button');
    expect(button.element.disabled).toBe(false);
  });
});

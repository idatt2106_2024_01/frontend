import { mount } from '@vue/test-utils';
import ProfileView from '@/views/ProfileView.vue';
import { describe, it, expect, beforeEach } from 'vitest';
import { createPinia, setActivePinia } from 'pinia';
import { createRouter, createMemoryHistory } from 'vue-router';

/**
 * Test for ProfileView
 */

/**
 * Defines the routes for the test
 */
const routes = [{ path: '/profile', component: ProfileView }];

/**
 * Creates a mock router
 */
const createMockRouter = () => {
  return createRouter({
    history: createMemoryHistory(),
    routes
  });
};

describe('Testing of ProfileView', () => {
  let router;

  beforeEach(async () => {
    setActivePinia(createPinia());
    router = createMockRouter();
    router.push('/profile');
    await router.isReady();
  });

  it('renders without crashing', () => {
    const wrapper = mount(ProfileView, {
      global: {
        plugins: [router],
        stubs: ['RouterLink', 'Input']
      }
    });
    expect(wrapper.exists()).toBe(true);
  });

  it('displays logout button', () => {
    const wrapper = mount(ProfileView, {
      global: {
        plugins: [router],
        stubs: ['RouterLink', 'Input']
      }
    });
    expect(wrapper.find('#logoutBtn').text()).toBe('Logg ut');
  });

  it('displays Card components', () => {
    const wrapper = mount(ProfileView, {
      global: {
        plugins: [router],
        stubs: ['RouterLink', 'Input']
      }
    });
    expect(wrapper.findComponent({ name: 'Card' })).toBeTruthy();
  });
});

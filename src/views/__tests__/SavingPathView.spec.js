import { mount } from '@vue/test-utils';
import SavingPathView from '@/views/SavingPathView.vue';
import { describe, it, expect, beforeEach } from 'vitest';
import { createPinia, setActivePinia } from 'pinia';
import { createRouter, createMemoryHistory } from 'vue-router';

/**
 * Test for SavingPathView
 */

/**
 * Defines the stubs for the test
 */
const globalStubs = [
  'RouterLink',
  'Button',
  'Popover',
  'PopoverTrigger',
  'PopoverContent',
  'Select',
  'SelectTrigger',
  'SelectContent',
  'SelectGroup',
  'SelectItem',
  'SelectLabel',
  'SelectValue',
  'Card',
  'Carousel',
  'CarouselContent',
  'CarouselItem',
  'CarouselNext',
  'CarouselPrevious',
  'Sparti',
  'NewSavingPath'
];

/**
 * Defines the routes for the test
 */
const routes = [{ path: '/saving-path', component: SavingPathView }];

/**
 * Creates a mock router
 */
const createMockRouter = () => {
  return createRouter({
    history: createMemoryHistory(),
    routes
  });
};

describe('Testing of SavingPathView', () => {
  let router;

  beforeEach(async () => {
    setActivePinia(createPinia());
    router = createMockRouter();
    await router.push('/saving-path');
    await router.isReady();
  });

  it('renders without crashing', () => {
    const wrapper = mount(SavingPathView, {
      global: {
        plugins: [router],
        stubs: [
          'RouterLink',
          'Button',
          'Popover',
          'PopoverTrigger',
          'PopoverContent',
          'Select',
          'SelectTrigger',
          'SelectContent',
          'SelectGroup',
          'SelectItem',
          'SelectLabel',
          'SelectValue',
          'Card',
          'Carousel',
          'CarouselContent',
          'CarouselItem',
          'CarouselNext',
          'CarouselPrevious',
          'Sparti',
          'NewSavingPath'
        ]
      }
    });
    expect(wrapper.exists()).toBe(true);
  });

  it('renders SavingPath component', () => {
    const wrapper = mount(SavingPathView, {
      global: {
        plugins: [router],
        stubs: ['RouterLink', 'Button']
      }
    });
    expect(wrapper.findComponent({ name: 'SavingPath' })).toBeTruthy();
  });

  it('correctly renders loading state', async () => {
    const wrapper = mount(SavingPathView, {
      global: {
        plugins: [router],
        stubs: [
          'RouterLink',
          'Button',
          'Popover',
          'PopoverTrigger',
          'PopoverContent',
          'Select',
          'SelectTrigger',
          'SelectContent',
          'SelectGroup',
          'SelectItem',
          'SelectLabel',
          'SelectValue',
          'Card',
          'Carousel',
          'CarouselContent',
          'CarouselItem',
          'CarouselNext',
          'CarouselPrevious',
          'Sparti',
          'NewSavingPath'
        ]
      },
      data() {
        return { loading: true };
      }
    });
    expect(wrapper.text()).toContain('Laster inn...');
  });

  it('formats deadlines correctly', () => {
    const date = '2021-12-31T00:00:00Z';
    const wrapper = mount(SavingPathView, {
      global: {
        plugins: [router],
        stubs: globalStubs
      }
    });
    expect(wrapper.vm.formattedDeadline(date)).toBe('31.12.2021');
  });
});

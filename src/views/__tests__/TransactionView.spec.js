import { mount } from '@vue/test-utils';
import TransactionsView from '@/views/TransactionView.vue';
import { describe, it, expect, vi, beforeEach } from 'vitest';
import { createPinia, setActivePinia } from 'pinia';
import { createRouter, createMemoryHistory } from 'vue-router';

/**
 * Test for TransactionsView
 */

/**
 * Defines the routes for the test
 */
const routes = [{ path: '/transaksjoner', component: TransactionsView }];

/**
 * Creates a mock router
 */
const createMockRouter = () => {
  return createRouter({
    history: createMemoryHistory(),
    routes
  });
};

describe('TransactionsView', () => {
  let router;

  beforeEach(async () => {
    setActivePinia(createPinia());
    router = createMockRouter();
    router.push('/transaksjoner');
    await router.isReady();
  });

  it('renders without crashing', () => {
    const wrapper = mount(TransactionsView, {
      global: {
        plugins: [router],
        stubs: ['RouterLink']
      }
    });
    expect(wrapper.exists()).toBe(true);
  });

  it('renders the correct number of invoice rows', () => {
    const wrapper = mount(TransactionsView, {
      global: {
        plugins: [router],
        stubs: ['RouterLink']
      }
    });
    expect(wrapper.findAll('.invoice-row').length).toBe(0);
  });

  it('renders the correct number of transaction rows', () => {
    const wrapper = mount(TransactionsView, {
      global: {
        plugins: [router],
        stubs: ['RouterLink']
      }
    });
    expect(wrapper.findAll('.transaction-row').length).toBe(0);
  });

  it('formats date correctly', () => {
    const isoString = '2024-05-03T00:00:00Z';
    const wrapper = mount(TransactionsView, {
      global: {
        plugins: [router],
        stubs: ['RouterLink']
      }
    });
    const formattedDate = wrapper.vm.formatDate(isoString);
    expect(formattedDate).toBe('03/05/24');
  });

  it('parses date string correctly', () => {
    const dateStr = '03/05/24';
    const wrapper = mount(TransactionsView, {
      global: {
        plugins: [router],
        stubs: ['RouterLink']
      }
    });
    const parsedDate = wrapper.vm.parseDateString(dateStr);
    const expectedDate = new Date(2024, 4, 3);

    expect(parsedDate.getFullYear()).toBe(expectedDate.getFullYear());
    expect(parsedDate.getMonth()).toBe(expectedDate.getMonth());
    expect(parsedDate.getDate()).toBe(expectedDate.getDate());
  });
});

const animate = require("tailwindcss-animate")

/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: ["class"],
  content: [
    './pages/**/*.{js,jsx,vue}',
    './components/**/*.{js,jsx,vue}',
    './app/**/*.{js,jsx,vue}',
    './src/**/*.{js,jsx,vue}',
	],
  prefix: "",
  theme: {
    container: {
      center: true,
      padding: "2rem",
      screens: {
        "2xl": "1400px",
      },
    },
    extend: {
      colors: {
        primary: 'var(--primary-color)',
        'primary-light': 'var(--primary-color-light)',
        'primary-dark': 'var(--primary-color-dark)',
        secondary: 'var(--secondary-color)',
        'secondary-light': 'var(--secondary-color-light)',
        'secondary-dark': 'var(--secondary-color-dark)',
        standard: 'var(--standard-color)',
        darkest: 'var(--text-color)',
        completed: 'var(--completed-color)',
        active: 'var(--active-color)',
        failed: 'var(--failed-color)',
        total: 'var(--total-color)',
      },
      keyframes: {
        "accordion-down": {
          from: { height: 0 },
          to: { height: "var(--radix-accordion-content-height)" },
        },
        "accordion-up": {
          from: { height: "var(--radix-accordion-content-height)" },
          to: { height: 0 },
        },
      },
      animation: {
        "accordion-down": "accordion-down 0.2s ease-out",
        "accordion-up": "accordion-up 0.2s ease-out",
      },
      width: {
        '11': '11rem',
        '9': '9rem',
      },
      height: {
        '11': '11rem',
        '9': '9rem',
      },
      fontFamily: {
        'gluten': ['Gluten'],
      },
      backgroundImage: theme => ({
        'PinkCardBg' : "url('@/assets/PinkThemeAssets/PinkCardBg.png')",
        'GreenCardBg' : "url('@/assets/GreenThemeAssets/GreenCardBg.png')",
      })
    },
  },
  plugins: [animate],
}